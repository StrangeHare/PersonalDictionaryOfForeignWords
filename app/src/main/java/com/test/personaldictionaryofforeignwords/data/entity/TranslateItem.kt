package com.test.personaldictionaryofforeignwords.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem.Companion.TRANSLATE_ITEM_TABLE_NAME

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
@Entity(tableName = TRANSLATE_ITEM_TABLE_NAME)
data class TranslateItem(
        @PrimaryKey()
        @ColumnInfo(name = MAIN_WORD_KEY)
        val word: String,
        @ColumnInfo(name = TRANSLATE_WORD_KEY)
        var translatedWord: String,
        @ColumnInfo(name = ITEM_DATE)
        var date: Long
) : IBaseEntity {
    companion object {
        const val ITEM_DATE = "item_date"
        const val TRANSLATE_ITEM_TABLE_NAME = "translate_item"
        const val MAIN_WORD_KEY = "main_word_key"
        const val TRANSLATE_WORD_KEY = "translate_word_key"
    }
}