package com.test.personaldictionaryofforeignwords.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.test.personaldictionaryofforeignwords.data.dao.TranslateDao
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
@Database(entities = [TranslateItem::class], version = 1)
abstract class DictionaryDB : RoomDatabase() {

    abstract fun translateDao(): TranslateDao

    companion object {

        private const val BD_NAME = "DictionaryDB"
        private var instance: DictionaryDB? = null

        @Synchronized
        fun get(context: Context?): DictionaryDB {
            if (instance == null && context != null) {
                instance = Room.databaseBuilder(context.applicationContext, DictionaryDB::class.java, BD_NAME)
                        .fallbackToDestructiveMigration()
                        .build()
            }
            return instance!!
        }
    }
}