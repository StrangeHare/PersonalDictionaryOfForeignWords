package com.test.personaldictionaryofforeignwords.data.dao

import android.arch.paging.DataSource
import android.arch.persistence.room.*
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem.Companion.ITEM_DATE
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem.Companion.MAIN_WORD_KEY
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem.Companion.TRANSLATE_ITEM_TABLE_NAME
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem.Companion.TRANSLATE_WORD_KEY

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
@Dao
interface TranslateDao {

    @Query("SELECT t.$MAIN_WORD_KEY, t.$TRANSLATE_WORD_KEY, t.$ITEM_DATE" +
            " FROM $TRANSLATE_ITEM_TABLE_NAME t" +
            " ORDER BY t.$ITEM_DATE")
    fun getAllWords(): DataSource.Factory<Int, TranslateItem>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: TranslateItem)

    @Delete
    fun delete(item: TranslateItem)

    @Query("SELECT t.$MAIN_WORD_KEY, t.$TRANSLATE_WORD_KEY, t.$ITEM_DATE" +
            " FROM $TRANSLATE_ITEM_TABLE_NAME t" +
            " WHERE t.$MAIN_WORD_KEY LIKE :search OR t.$TRANSLATE_WORD_KEY LIKE :search")
    fun getSavedWord(search: String): DataSource.Factory<Int, TranslateItem>
}