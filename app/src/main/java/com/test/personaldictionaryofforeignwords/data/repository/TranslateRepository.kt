package com.test.personaldictionaryofforeignwords.data.repository

import com.test.personaldictionaryofforeignwords.data.daataSource.DataSource
import com.test.personaldictionaryofforeignwords.data.daataSource.IDataSource
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem
import com.test.personaldictionaryofforeignwords.newtwork.ApiCreator
import com.test.personaldictionaryofforeignwords.newtwork.ApiCreator.API_KEY
import com.test.personaldictionaryofforeignwords.newtwork.TranslateApi

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
class TranslateRepository(
        private val api: TranslateApi = ApiCreator.createApi(),
        private val dataSource: IDataSource = DataSource()
) : ITranslateRepository {

    override fun detectLanguage(text: String) = api.detect(API_KEY, text)
    override fun getAllWords() = dataSource.getAllWords()
    override fun translateWord(pair: Pair<String, String>) = api.translate(API_KEY, pair.first, pair.second)
    override fun getSavedWord(searchWord: String) = dataSource.getWord(searchWord)
    override fun saveWord(pair: Pair<String, String>) {
        dataSource.putWord(TranslateItem(pair.first, pair.second, date = System.currentTimeMillis() / 1000))
    }

    override fun deleteWord(item: TranslateItem) {
        dataSource.deleteWord(item)
    }
}