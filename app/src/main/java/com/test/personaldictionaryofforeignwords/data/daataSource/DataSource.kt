package com.test.personaldictionaryofforeignwords.data.daataSource

import android.arch.paging.LivePagedListBuilder
import com.test.personaldictionaryofforeignwords.App
import com.test.personaldictionaryofforeignwords.data.DictionaryDB
import com.test.personaldictionaryofforeignwords.data.dao.TranslateDao
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem

/**
 *
 * @author StrangeHare
 *         Date: 16.05.2018
 */
class DataSource(
        private val dao: TranslateDao = DictionaryDB.get(App.instance).translateDao()
) : IDataSource {

    override fun getAllWords() = LivePagedListBuilder(dao.getAllWords(), 20).build()
    override fun getWord(search: String) = LivePagedListBuilder(dao.getSavedWord(search), 20).build()
    override fun deleteWord(item: TranslateItem) {
        dao.delete(item)
    }

    override fun putWord(item: TranslateItem) {
        dao.insert(item)
    }
}