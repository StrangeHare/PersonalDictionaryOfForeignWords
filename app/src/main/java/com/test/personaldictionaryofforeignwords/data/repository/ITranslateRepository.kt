package com.test.personaldictionaryofforeignwords.data.repository

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem
import com.test.personaldictionaryofforeignwords.newtwork.model.Detected
import com.test.personaldictionaryofforeignwords.newtwork.model.Translate
import io.reactivex.Observable

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
interface ITranslateRepository {

    fun translateWord(pair: Pair<String, String>): Observable<Translate>
    fun getSavedWord(searchWord: String): LiveData<PagedList<TranslateItem>>
    fun saveWord(pair: Pair<String, String>)
    fun getAllWords(): LiveData<PagedList<TranslateItem>>
    fun detectLanguage(text: String): Observable<Detected>
    fun deleteWord(item: TranslateItem)
}