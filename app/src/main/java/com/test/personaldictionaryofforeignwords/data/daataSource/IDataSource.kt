package com.test.personaldictionaryofforeignwords.data.daataSource

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem

/**
 *
 * @author StrangeHare
 *         Date: 16.05.2018
 */
interface IDataSource {

    fun getAllWords(): LiveData<PagedList<TranslateItem>>
    fun getWord(search: String): LiveData<PagedList<TranslateItem>>
    fun putWord(item: TranslateItem)
    fun deleteWord(item: TranslateItem)
}