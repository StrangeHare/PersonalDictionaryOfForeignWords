package com.test.personaldictionaryofforeignwords

import android.app.Activity
import android.app.Application
import com.test.personaldictionaryofforeignwords.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 * Date: 15.05.2018
 */
open class App : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        instance = this
        DaggerAppComponent.builder()
                .setContext(instance)
                .build()
                .inject(instance)
    }

    override fun activityInjector(): AndroidInjector<Activity>? {
        return dispatchingAndroidInjector
    }

    companion object {
        lateinit var instance: App
    }
}
