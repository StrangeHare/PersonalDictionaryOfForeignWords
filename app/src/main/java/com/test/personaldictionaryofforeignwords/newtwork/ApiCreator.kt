package com.test.personaldictionaryofforeignwords.newtwork

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
object ApiCreator {
    const val API_KEY = "trnsl.1.1.20180515T193126Z.0f86ef8b163f615d.142daf93bf74e8d64360e67d0ce9eaa54c06160e"
    private val clientBuilder = OkHttpClient.Builder()
    private var gson = GsonBuilder()
            .setLenient()
            .create()
    private val retrofitBuilder = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))

    fun createApi(): TranslateApi {
        return initRetrofit().create(TranslateApi::class.java)
    }

    private fun initRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return retrofitBuilder
                .baseUrl("https://translate.yandex.net")
                .client(clientBuilder.addInterceptor(interceptor).build())
                .build()
    }
}
