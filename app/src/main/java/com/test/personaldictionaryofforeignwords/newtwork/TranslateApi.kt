package com.test.personaldictionaryofforeignwords.newtwork

import com.test.personaldictionaryofforeignwords.newtwork.model.Detected
import com.test.personaldictionaryofforeignwords.newtwork.model.Translate
import io.reactivex.Observable
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
interface TranslateApi {

    @Headers("Content-Type: application/json")
    @POST("/api/v1.5/tr.json/translate")
    fun translate(@Query("key") apiKey: String,
                  @Query("text") text: String,
                  @Query("lang") lang: String) : Observable<Translate>

    @Headers("Content-Type: application/json")
    @POST("/api/v1.5/tr.json/detect")
    fun detect(@Query("key") apiKey: String,
                  @Query("text") text: String) : Observable<Detected>
}
