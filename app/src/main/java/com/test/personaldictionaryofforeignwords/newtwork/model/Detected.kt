package com.test.personaldictionaryofforeignwords.newtwork.model

import com.google.gson.annotations.SerializedName

data class Detected(
        @SerializedName("code")
        val code: Int? = null,
        @SerializedName("lang")
        val lang: String? = null
)
