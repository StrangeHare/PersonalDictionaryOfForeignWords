package com.test.personaldictionaryofforeignwords.newtwork.model

import com.google.gson.annotations.SerializedName

data class Translate(
        @SerializedName("code")
        val code: Int? = null,
        @SerializedName("text")
        val text: List<String?>? = null,
        @SerializedName("lang")
        val lang: String? = null
)
