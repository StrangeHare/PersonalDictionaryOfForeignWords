package com.test.personaldictionaryofforeignwords.di.module.activity

import com.test.personaldictionaryofforeignwords.di.scope.ActivityScope
import com.test.personaldictionaryofforeignwords.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
@Module(includes = [AndroidSupportInjectionModule::class])
interface ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    fun mainProviderActivityInjector(): MainActivity
}