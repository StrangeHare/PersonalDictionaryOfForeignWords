package com.test.personaldictionaryofforeignwords.di.module.activity

import android.arch.lifecycle.ViewModelProviders
import com.test.personaldictionaryofforeignwords.view.MainActivity
import com.test.personaldictionaryofforeignwords.viewmodel.ViewModel
import dagger.Module
import dagger.Provides

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
@Module
class MainActivityModule {

    @Provides
    fun provideViewModel(activity: MainActivity): ViewModel {
        return ViewModelProviders.of(activity).get(ViewModel::class.java)
    }
}