package com.test.personaldictionaryofforeignwords.di.component

import android.content.Context
import com.test.personaldictionaryofforeignwords.App
import com.test.personaldictionaryofforeignwords.di.module.activity.ActivityModule
import com.test.personaldictionaryofforeignwords.di.module.fragment.FragmentModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 *
 * @author StrangeHare
 * Date: 15.05.2018
 */
@Singleton
@Component(modules = [ActivityModule::class, FragmentModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun setContext(context: Context): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}