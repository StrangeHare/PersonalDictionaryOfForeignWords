package com.test.personaldictionaryofforeignwords.di.module.fragment

import android.arch.lifecycle.ViewModelProviders
import com.test.personaldictionaryofforeignwords.view.feed.FeedAdapter
import com.test.personaldictionaryofforeignwords.view.feed.FeedFragment
import com.test.personaldictionaryofforeignwords.viewmodel.ViewModel
import dagger.Module
import dagger.Provides

/**
 *
 * @author StrangeHare
 *         Date: 17.05.2018
 */
@Module
class FeedFragmentModule {

    @Provides
    fun provideViewModel(fragment: FeedFragment): ViewModel {
        return ViewModelProviders.of(fragment.activity!!).get(ViewModel::class.java)
    }

    @Provides
    fun provideFeedAdapter(): FeedAdapter {
        return FeedAdapter()
    }
}