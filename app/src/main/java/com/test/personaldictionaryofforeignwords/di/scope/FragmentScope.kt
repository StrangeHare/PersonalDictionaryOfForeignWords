package com.test.personaldictionaryofforeignwords.di.scope

import javax.inject.Scope

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScope
