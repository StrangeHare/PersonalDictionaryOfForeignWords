package com.test.personaldictionaryofforeignwords.di.module.fragment

import android.arch.lifecycle.ViewModelProviders
import com.test.personaldictionaryofforeignwords.view.search.SearchFragment
import com.test.personaldictionaryofforeignwords.viewmodel.ViewModel
import dagger.Module
import dagger.Provides

/**
 *
 * @author StrangeHare
 *         Date: 17.05.2018
 */
@Module
class SearchFragmentModule {

    @Provides
    fun provideViewModel(fragment: SearchFragment): ViewModel {
        return ViewModelProviders.of(fragment.activity!!).get(ViewModel::class.java)
    }
}