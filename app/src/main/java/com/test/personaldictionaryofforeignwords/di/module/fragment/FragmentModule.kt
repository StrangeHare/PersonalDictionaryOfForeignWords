package com.test.personaldictionaryofforeignwords.di.module.fragment

import com.test.personaldictionaryofforeignwords.di.scope.FragmentScope
import com.test.personaldictionaryofforeignwords.view.createform.CreateFormFragment
import com.test.personaldictionaryofforeignwords.view.feed.FeedFragment
import com.test.personaldictionaryofforeignwords.view.search.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
@Module(includes = [AndroidSupportInjectionModule::class])
interface FragmentModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [CreateFormFragmentModule::class])
    fun createFormFragmentInjector(): CreateFormFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [FeedFragmentModule::class])
    fun feedFragmentInjector(): FeedFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [SearchFragmentModule::class])
    fun searchFragmentInjector(): SearchFragment
}