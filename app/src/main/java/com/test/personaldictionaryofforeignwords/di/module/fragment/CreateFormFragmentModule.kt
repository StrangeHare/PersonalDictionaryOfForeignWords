package com.test.personaldictionaryofforeignwords.di.module.fragment

import android.arch.lifecycle.ViewModelProviders
import com.test.personaldictionaryofforeignwords.view.createform.CreateFormFragment
import com.test.personaldictionaryofforeignwords.viewmodel.ViewModel
import dagger.Module
import dagger.Provides

/**
 *
 * @author StrangeHare
 *         Date: 17.05.2018
 */
@Module
class CreateFormFragmentModule {

    @Provides
    fun provideViewModel(fragment: CreateFormFragment): ViewModel {
        return ViewModelProviders.of(fragment.activity!!).get(ViewModel::class.java)
    }
}