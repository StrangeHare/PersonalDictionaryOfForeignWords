package com.test.personaldictionaryofforeignwords.base

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
open class BaseViewModel : ViewModel(), LifecycleObserver {

    protected var disposables = CompositeDisposable()

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onDestroy() {
        disposables.clear()
    }
}
