package com.test.personaldictionaryofforeignwords.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.DaggerFragment

/**
 *
 * @author StrangeHare
 *         Date: 17.05.2018
 */
abstract class BaseFragment<B : ViewDataBinding> : DaggerFragment() {

    protected lateinit var binding: B

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return setAndBindContentView(inflater, container)
    }

    @LayoutRes
    abstract fun getLayoutId(): Int

    private fun setAndBindContentView(inflater: LayoutInflater, parent: ViewGroup?): View {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), parent, false)
        return binding.root
    }
}
