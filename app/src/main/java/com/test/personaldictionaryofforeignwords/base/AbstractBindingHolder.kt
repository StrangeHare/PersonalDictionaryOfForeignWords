package com.test.personaldictionaryofforeignwords.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.View
import com.test.personaldictionaryofforeignwords.data.entity.IBaseEntity

/**
 *
 * @author StrangeHare
 *         Date: 17.05.2018
 */
abstract class AbstractBindingHolder<B : ViewDataBinding>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    protected lateinit var binding: B

    init {
        setAndBindContentView()
    }

    abstract fun bindTo(entity: IBaseEntity?)

    private fun setAndBindContentView() {
        binding = DataBindingUtil.bind(itemView)!!
    }
}