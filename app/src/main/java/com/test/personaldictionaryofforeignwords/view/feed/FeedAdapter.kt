package com.test.personaldictionaryofforeignwords.view.feed

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.view.ViewGroup
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem

/**
 *
 * @author StrangeHare
 *         Date: 16.05.2018
 */
class FeedAdapter : PagedListAdapter<TranslateItem, FeedViewHolder>((object : DiffUtil.ItemCallback<TranslateItem>() {

    override fun areItemsTheSame(oldItem: TranslateItem, newItem: TranslateItem): Boolean =
            oldItem.word == newItem.word

    override fun areContentsTheSame(oldItem: TranslateItem, newItem: TranslateItem): Boolean =
            oldItem == newItem
})) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = FeedViewHolder(parent)

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }
}