package com.test.personaldictionaryofforeignwords.view.createform

import com.test.personaldictionaryofforeignwords.newtwork.model.Translate

/**
 *
 * @author StrangeHare
 *         Date: 17.05.2018
 */
interface CreateFormFragmentView {

    fun onTranslate(translate: Translate)
    fun onDetected(text: String, detectedLang: String?)
    fun onRequestError()
    fun onSearch()
    fun onShowCreateFrom()

}