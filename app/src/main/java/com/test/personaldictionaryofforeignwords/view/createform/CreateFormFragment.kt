package com.test.personaldictionaryofforeignwords.view.createform

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.test.personaldictionaryofforeignwords.R
import com.test.personaldictionaryofforeignwords.base.BaseFragment
import com.test.personaldictionaryofforeignwords.databinding.FragmentCreateFormBinding
import com.test.personaldictionaryofforeignwords.newtwork.model.Translate
import com.test.personaldictionaryofforeignwords.utils.hideKeyboard
import com.test.personaldictionaryofforeignwords.utils.onTextChanged
import com.test.personaldictionaryofforeignwords.utils.showKeyboard
import com.test.personaldictionaryofforeignwords.utils.toast
import com.test.personaldictionaryofforeignwords.viewmodel.ViewModel
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 16.05.2018
 */
class CreateFormFragment : BaseFragment<FragmentCreateFormBinding>(),
        CreateFormFragmentView, AdapterView.OnItemSelectedListener, View.OnClickListener {

    @Inject
    lateinit var viewModel: ViewModel
    private val hmLang = hashMapOf("Русский" to "ru", "Английский" to "en", "Французкий" to "fr")

    override fun getLayoutId() = R.layout.fragment_create_form

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setCreateFormFragmentView(this)
        with(binding) {
            editTextEnterWord.onTextChanged { p0, _, _, _ ->
                viewModel.detectLanguage(p0.toString())
                clearTranslate()
            }
            buttonSave.setOnClickListener(this@CreateFormFragment)
        }
        with(binding.spinner) {
            onItemSelectedListener = this@CreateFormFragment
            val languages = resources.getStringArray(R.array.languages)
            val spinnerAdapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, languages.toCollection(ArrayList<String>()))
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            adapter = spinnerAdapter
            setSelection(spinnerAdapter.getPosition(languages[0]))
        }
    }

    override fun onDetected(text: String, detectedLang: String?) {
        viewModel.onSearchTextChanged(Pair(text, detectedLang))
    }

    override fun onTranslate(translate: Translate) {
        binding.textViewTranslate.text = translate.text?.joinToString()
        clearTranslate()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        // Nothing
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (parent?.id == binding.spinner.id) {
            viewModel.setLanguageOfTranslate(hmLang[parent.getItemAtPosition(position).toString()])
            viewModel.detectLanguage(binding.editTextEnterWord.text.toString())
        }
    }

    override fun onShowCreateFrom() {
        with(binding.scrollView) {
            visibility = if (visibility == View.VISIBLE) View.GONE else View.VISIBLE
            this.postDelayed({
                if (visibility == View.GONE) {
                    hideKeyboard(this)
                } else {
                    with(binding.editTextEnterWord) {
                        requestFocus()
                        showKeyboard(this)
                    }
                }
            }, 300)
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v?.id) {
                buttonSave.id -> {
                    viewModel.saveWord(Pair(editTextEnterWord.text.toString(), textViewTranslate.text.toString()))
                    editTextEnterWord.text.clear()
                    textViewTranslate.text = ""
                }
            }
        }
    }

    override fun onRequestError() {
        context?.toast(getString(R.string.error_request_translate))
    }

    override fun onSearch() {
        binding.scrollView.visibility = View.GONE
        hideKeyboard(binding.editTextEnterWord)
    }

    private fun clearTranslate() {
        if (binding.editTextEnterWord.text.isBlank()) {
            binding.textViewTranslate.text = ""
        }
    }

    companion object {

        const val TAG = "CreateFormFragment"

        @JvmStatic
        fun newInstance() = CreateFormFragment()
    }
}
