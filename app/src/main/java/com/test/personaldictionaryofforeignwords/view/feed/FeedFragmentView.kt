package com.test.personaldictionaryofforeignwords.view.feed

/**
 *
 * @author StrangeHare
 *         Date: 19.05.2018
 */
interface FeedFragmentView {

    fun onSaveWord()
    fun onRequestError()
    fun onSearch(query: String)
}