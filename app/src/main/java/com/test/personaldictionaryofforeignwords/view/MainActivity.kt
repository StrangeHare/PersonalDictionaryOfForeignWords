package com.test.personaldictionaryofforeignwords.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.test.personaldictionaryofforeignwords.R
import com.test.personaldictionaryofforeignwords.databinding.ActivityMainBinding
import com.test.personaldictionaryofforeignwords.utils.addFragment
import com.test.personaldictionaryofforeignwords.view.createform.CreateFormFragment
import com.test.personaldictionaryofforeignwords.view.feed.FeedFragment
import com.test.personaldictionaryofforeignwords.view.search.SearchFragment
import com.test.personaldictionaryofforeignwords.viewmodel.ViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 16.05.2018
 */
class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModel: ViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(viewModel)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        val createFormFragment = supportFragmentManager.findFragmentByTag(CreateFormFragment.TAG) as CreateFormFragment?
        val searchFragment = supportFragmentManager.findFragmentByTag(SearchFragment.TAG) as SearchFragment?
        val feedFormFragment = supportFragmentManager.findFragmentByTag(FeedFragment.TAG) as FeedFragment?

        if (createFormFragment == null) addFragment(CreateFormFragment.newInstance(), binding.createFormContainer.id, CreateFormFragment.TAG)
        if (searchFragment == null) addFragment(SearchFragment.newInstance(), binding.searchContainer.id, SearchFragment.TAG)
        if (feedFormFragment == null) addFragment(FeedFragment.newInstance(), binding.feedContainer.id, FeedFragment.TAG)
    }
}
