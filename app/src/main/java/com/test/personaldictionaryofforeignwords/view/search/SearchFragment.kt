package com.test.personaldictionaryofforeignwords.view.search

import android.os.Bundle
import android.support.v7.widget.SearchView
import android.view.View
import com.test.personaldictionaryofforeignwords.R
import com.test.personaldictionaryofforeignwords.base.BaseFragment
import com.test.personaldictionaryofforeignwords.databinding.FragmentSearchBinding
import com.test.personaldictionaryofforeignwords.viewmodel.ViewModel
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 16.05.2018
 */
class SearchFragment : BaseFragment<FragmentSearchBinding>(), SearchView.OnQueryTextListener, View.OnClickListener {

    @Inject
    lateinit var viewModel: ViewModel

    override fun getLayoutId() = R.layout.fragment_search

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            searchView.setOnQueryTextListener(this@SearchFragment)
            buttonAdd.setOnClickListener(this@SearchFragment)
        }
    }

    override fun onQueryTextSubmit(query: String?) = false

    override fun onQueryTextChange(newText: String?): Boolean {
        viewModel.replaceSubscription(newText.toString())
        return false
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v?.id) {
                buttonAdd.id -> viewModel.showCreateForm()
            }
        }
    }

    companion object {

        const val TAG = "SearchFragment"

        @JvmStatic
        fun newInstance() = SearchFragment()
    }
}
