package com.test.personaldictionaryofforeignwords.view.feed

import android.view.LayoutInflater
import android.view.ViewGroup
import com.test.personaldictionaryofforeignwords.R
import com.test.personaldictionaryofforeignwords.base.AbstractBindingHolder
import com.test.personaldictionaryofforeignwords.data.entity.IBaseEntity
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem
import com.test.personaldictionaryofforeignwords.databinding.HolderWordBinding
import com.test.personaldictionaryofforeignwords.view.MainActivity

/**
 *
 * @author StrangeHare
 *         Date: 16.05.2018
 */
class FeedViewHolder(parent: ViewGroup) : AbstractBindingHolder<HolderWordBinding>(
        LayoutInflater.from(parent.context).inflate(R.layout.holder_word, parent, false)
) {
    lateinit var item: TranslateItem

    init {
        binding.buttonDelete.setOnClickListener {
            (itemView.context as MainActivity).viewModel.delete(item)
        }
    }

    override fun bindTo(entity: IBaseEntity?) {
        item = entity as TranslateItem
        binding.item = item
    }
}