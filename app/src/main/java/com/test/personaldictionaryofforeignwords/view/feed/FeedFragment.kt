package com.test.personaldictionaryofforeignwords.view.feed

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.test.personaldictionaryofforeignwords.R
import com.test.personaldictionaryofforeignwords.base.BaseFragment
import com.test.personaldictionaryofforeignwords.databinding.FragmentFeedBinding
import com.test.personaldictionaryofforeignwords.utils.toast
import com.test.personaldictionaryofforeignwords.viewmodel.ViewModel
import javax.inject.Inject

/**
 *
 * @author StrangeHare
 *         Date: 16.05.2018
 */
class FeedFragment : BaseFragment<FragmentFeedBinding>(), FeedFragmentView {

    @Inject
    lateinit var viewModel: ViewModel
    @Inject
    lateinit var feedAdapter: FeedAdapter

    override fun getLayoutId() = R.layout.fragment_feed

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setFeedFragmentView(this)
        startListener()

        val lm = LinearLayoutManager(view.context).apply {
            reverseLayout = true
            stackFromEnd = true
        }

        with(binding.recyclerView) {
            adapter = feedAdapter
            layoutManager = lm
            addItemDecoration(DividerItemDecoration(view.context, LinearLayout.VERTICAL))
        }
    }

    override fun onSaveWord() {
        binding.recyclerView.postDelayed({
            binding.recyclerView.scrollToPosition(feedAdapter.itemCount - 1)
        }, 300)
    }

    override fun onRequestError() {
        context?.toast(getString(R.string.error_request_translate))
    }

    override fun onSearch(query: String) {
        viewModel.getLiveResults().removeObservers(this)
        if (query.isBlank()) {
            viewModel.setLiveResult(viewModel.getAllWords())
            startListener()
            return
        }
        viewModel.setLiveResult(viewModel.getSavedWord(query))
        startListener()
    }

    private fun startListener() {
        viewModel.getLiveResults().observe(this, Observer(feedAdapter::submitList))
    }

    companion object {

        const val TAG = "FeedFragment"

        @JvmStatic
        fun newInstance() = FeedFragment()
    }
}
