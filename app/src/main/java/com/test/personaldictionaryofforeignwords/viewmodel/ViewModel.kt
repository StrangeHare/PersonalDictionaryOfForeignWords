package com.test.personaldictionaryofforeignwords.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.test.personaldictionaryofforeignwords.base.BaseViewModel
import com.test.personaldictionaryofforeignwords.data.entity.TranslateItem
import com.test.personaldictionaryofforeignwords.data.repository.ITranslateRepository
import com.test.personaldictionaryofforeignwords.data.repository.TranslateRepository
import com.test.personaldictionaryofforeignwords.view.createform.CreateFormFragmentView
import com.test.personaldictionaryofforeignwords.view.feed.FeedFragmentView
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 *
 * @author StrangeHare
 *         Date: 15.05.2018
 */
class ViewModel : BaseViewModel() {

    private val repository: ITranslateRepository = TranslateRepository()
    private val queryPublishSubject = PublishSubject.create<Pair<String, String?>>()
    private var liveResults: LiveData<PagedList<TranslateItem>> = getAllWords()

    private lateinit var createFormView: CreateFormFragmentView
    private lateinit var feedFragmentView: FeedFragmentView
    private lateinit var lang: String

    init {
        queryPublishSubject.debounce(500, TimeUnit.MILLISECONDS)
                .filter({ it.first.isNotBlank() })
                .subscribeOn(Schedulers.io())
                .flatMap({ needTranslate -> repository.translateWord(Pair(needTranslate.first, "${needTranslate.second}-$lang")) })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    createFormView.onTranslate(it)
                }, {
                    createFormView.onRequestError()
                })
    }

    fun setCreateFormFragmentView(view: CreateFormFragmentView) {
        createFormView = view
    }

    fun setFeedFragmentView(view: FeedFragmentView) {
        feedFragmentView = view
    }

    fun setLanguageOfTranslate(lang: String?) {
        this.lang = lang ?: "en"
    }

    fun onSearchTextChanged(query: Pair<String, String?>) {
        queryPublishSubject.onNext(query)
    }

    fun getLiveResults() = liveResults

    fun setLiveResult(newResult: LiveData<PagedList<TranslateItem>>) {
        liveResults = newResult
    }

    fun getSavedWord(searchWord: String) = repository.getSavedWord(searchWord)

    fun getAllWords() = repository.getAllWords()

    fun saveWord(pair: Pair<String, String>) {
        disposables.add(Completable.fromCallable { repository.saveWord(pair) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    feedFragmentView.onSaveWord()
                }, {
                    feedFragmentView.onRequestError()
                }))
    }

    fun delete(item: TranslateItem) {
        disposables.add(Completable.fromCallable { repository.deleteWord(item) }
                .subscribeOn(Schedulers.io())
                .subscribe())
    }

    fun detectLanguage(query: String) {
        if (query.isBlank()) {
            return
        }
        disposables.add(repository.detectLanguage(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    createFormView.onDetected(query, it.lang)
                }, {
                    createFormView.onRequestError()
                }))
    }

    fun replaceSubscription(query: String) {
        feedFragmentView.onSearch(query)
        createFormView.onSearch()
    }

    fun showCreateForm() {
        createFormView.onShowCreateFrom()
    }
}