package com.test.personaldictionaryofforeignwords

import android.support.test.runner.AndroidJUnit4
import android.widget.EditText
import com.robotium.solo.Solo
import com.test.personaldictionaryofforeignwords.view.MainActivity
import com.test.personaldictionaryofforeignwords.view.createform.CreateFormFragment
import com.test.personaldictionaryofforeignwords.view.feed.FeedFragment
import com.test.personaldictionaryofforeignwords.view.search.SearchFragment
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 *
 * @author StrangeHare
 *         Date: 20.05.2018
 */
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule @JvmField
    var activityRule: MainActivityTestRule = MainActivityTestRule(MainActivity::class.java)
    private lateinit var solo: Solo
    private val activityMain = MainActivity::class.java.simpleName

    @Before
    @Throws(Exception::class)
    fun setUp() {
        solo = Solo(activityRule.getInstrumentation(), activityRule.activity)
    }

    @Test(timeout = 20000)
    @Throws(Exception::class)
    fun checkIfMainActivityIsProperlyDisplayed() {
        solo.assertCurrentActivity(activityRule.activity.getString(R.string.error_no_class_def_found, activityMain), activityMain)
        solo.currentActivity.fragmentManager.findFragmentByTag(FeedFragment.TAG)
        checkSearchFragment()
        checkCreateFormFragment()
    }

    private fun checkSearchFragment() {
        solo.currentActivity.fragmentManager.findFragmentByTag(SearchFragment.TAG)
        solo.waitForView(R.id.button_add)
        solo.clickOnView(solo.getView(R.id.button_add))
        solo.waitForView(R.id.search_view)
        solo.getView(R.id.search_view)
    }

    private fun checkCreateFormFragment() {
        val fieldValue = "Testing Text"
        solo.currentActivity.fragmentManager.findFragmentByTag(CreateFormFragment.TAG)
        val editText = solo.getView(R.id.edit_text_enter_word)
        solo.enterText(editText as EditText, fieldValue)
        assertEquals("Text should be the field value", fieldValue, editText.text.toString())
        solo.clickOnView(solo.getView(R.id.button_save))
    }
}