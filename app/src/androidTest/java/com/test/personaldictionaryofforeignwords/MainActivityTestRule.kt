package com.test.personaldictionaryofforeignwords

import android.app.Instrumentation
import android.support.test.InstrumentationRegistry
import android.support.test.rule.ActivityTestRule
import com.robotium.solo.Solo
import com.test.personaldictionaryofforeignwords.view.MainActivity

/**
 *
 * @author StrangeHare
 *         Date: 20.05.2018
 */
open class MainActivityTestRule(activityClass: Class<MainActivity>?) : ActivityTestRule<MainActivity>(activityClass) {

    private lateinit var solo: Solo

    @Throws(Exception::class)
    fun setUp() {
        solo = Solo(getInstrumentation(), activity)
    }

    @Throws(Exception::class)
    fun tearDown() {
        solo.finishOpenedActivities()
    }

    fun getInstrumentation(): Instrumentation = InstrumentationRegistry.getInstrumentation()
}